import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, Button, TextInput, ToastAndroid, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const request = async () => {

  var response = await fetch("https://bitbucket.org/itesmguillermorivas/partial2/raw/45f22905941b70964102fce8caf882b51e988d23/carros.json");
  var json = await response.json();

  console.log(json);
  console.log(json[0]);
  console.log(json[0].marca);

}

const alertaYRequest = (mensaje: string) => {

  alert(mensaje);
  console.log(mensaje);
  request();
  //ToastAndroid.show(mensaje, ToastAndroid.SHORT);
  
}

const Perrito = (props:any) => {

  // 2 tipos de atributos para los componentes
  // 1 - props: argumentos que vienen del exterior y se pueden utilizar internamente
  // 2 - estados: propiedades internas del componente que pueden cambiar de valor

  const[estaFeliz, setEstaFeliz] = useState(false);
  const[textito, setTextito] = useState("");

  return (
    <View>
      <Text>woof woof! me llamo {props.nombre} y estoy {estaFeliz? "FELIZ! :D": "TRISTE :("}</Text>
      <Image 
        source={{uri: props.uri}}
        style={{width:100, height:100}}
      />
      <Button 
        title="cambiar estado de animo"
        onPress={() => {
          setEstaFeliz(!estaFeliz);
        }}
      />
      <TextInput 
        placeholder="pon el saludo del perrito"
        onChangeText={text => {
          setTextito(text);
        }}
      />
      <Text>woof! {textito}</Text>
      <Button
        title="Mathew para presidente"
        onPress={() => {

          alertaYRequest("VOTEN TODOS!");
        }} 
      />
      
    </View>
  );
}

const Principal = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text>Un texto chido</Text>
      <Button
        title="Cambiar a detalle"
        onPress={() => {
          navigation.navigate("Detalle", {datosChidos: "información extremadamente importante"});
        }} 
      />
      <FlatList 
      
        data={[
          {nombre: "juanito", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "clodomiro", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "romina", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "pancracio", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"},
          {nombre: "gertrudio", uri: "http://4.bp.blogspot.com/-7R8yr5qhV8Q/UPFlZvJCB4I/AAAAAAAAI7s/f-CYwM5F6k4/s1600/puppy.jpg"}
        ]}
        renderItem={({item}) => 
          <Perrito 
            nombre={item.nombre}
            uri={item.uri}
          />
        }
      />

      <StatusBar style="auto" />
    </View>
  );
}

const Detalle = ({navigation, route}) => {
  return(
  <View style={styles.container}>
      <Text>Una vista de detalle con estos datos: {route.params.datosChidos}</Text>
      
      <StatusBar style="auto" />
    </View>
    );
}

const App = () => {
  return(
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen 
        name="Principal"
        component={Principal}
      />
      <Stack.Screen 
        name="Detalle"
        component={Detalle}
      />
    </Stack.Navigator>

  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


export default App;